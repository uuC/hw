
function getTime (){
    let d = new Date();
    return d.toLocaleTimeString([], {timeStyle: 'short'});
}

const input = document.getElementById("input")
//one line

function addList1(form){
    let notCompleted = document.getElementById("notCompleted");
    let completed = document.getElementById("completed");

    let newLiElement = document.createElement("li");
    let timeBox = document.createTextNode(getTime());
    let checkButton = document.createElement("button");
    let delButton = document.createElement("button");
    let span = document.createElement("span");

    checkButton.textContent = "Done";
    delButton.textContent = "Trash";
    checkButton.id = "done";
    delButton.id = "trash";
    timeBox.id = "time";

    if(input.value !== ""){
        newLiElement.textContent = input.value;
        input.value = "";
        
        notCompleted.appendChild(newLiElement);
        newLiElement.appendChild(span);
        newLiElement.appendChild(checkButton);
        newLiElement.appendChild(delButton);
        span.appendChild(timeBox);
       
    }

    checkButton.addEventListener("click", function(){
        const parent = this.parentNode;
        completed.appendChild(parent);
        checkButton.style.display = "none";
    });

    delButton.addEventListener("click", function(){
        const parent = this.parentNode;
        parent.remove();
    } )
}


